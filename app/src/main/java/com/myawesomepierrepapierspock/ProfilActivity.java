package com.myawesomepierrepapierspock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.myawesomepierrepapierspock.model.User;
import com.myawesomepierrepapierspock.model.Login;

public class ProfilActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent lastIntent = new Intent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        final TextView tvMail = findViewById(R.id.tvMail);
        final TextView tvName = findViewById(R.id.tvName);
        final TextView tvAge = findViewById(R.id.tvAge);
        final TextView tvGagner = findViewById(R.id.tvGagner);
        final TextView tvJouer = findViewById(R.id.tvJouer);
        final TextView tvSexe = findViewById(R.id.tvSexe);
        final TextView tvPoints = findViewById(R.id.tvPoint);
        final TextView tvPseudo = findViewById(R.id.tvPseudo);
        final Button classement = findViewById(R.id.btClassement);
        final Button home = findViewById(R.id.btHome);
        final Button Profil = findViewById(R.id.btProfil);
        final Login usser = new Login();
        usser.AlreadySignedin();

        Log.d("profil", usser.getUser().toString());


        tvMail.setText(usser.getUser().getMail());
        tvName.setText(usser.getUser().getName());
        tvPseudo.setText(usser.getUser().getPseudo());
        tvPoints.setText( Integer.toString(usser.getUser().getPoint()));
        tvGagner.setText( Integer.toString(usser.getUser().getPerdu()));
        tvSexe.setText(usser.getUser().getSexe());
        tvAge.setText(usser.getUser().getAge().toString());
        tvJouer.setText( Integer.toString(usser.getUser().getJouer()));
        Profil.setText("Actualise");

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfilActivity.this, MenuActivity.class));
            }
        });

        classement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfilActivity.this, RankActivity.class));
            }
        });

        Profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvMail.setText(usser.getUser().getMail());
                tvName.setText(usser.getUser().getName());
                tvPseudo.setText(usser.getUser().getPseudo());
                tvPoints.setText( Integer.toString(usser.getUser().getPoint()));
                tvGagner.setText( Integer.toString(usser.getUser().getPerdu()));
                tvSexe.setText(usser.getUser().getSexe());
                tvAge.setText(usser.getUser().getAge().toString());
                tvJouer.setText( Integer.toString(usser.getUser().getJouer()));

            }
        });
    }
}
