package com.myawesomepierrepapierspock.Play;

import android.util.Log;

import java.util.Random;

public class Game {
    private Integer Diffic =0;
    private Integer Etape=0;
    private Integer nbrEtapeMax=5;
    private Integer PointServeur=0;
    private Integer PointClient=0;
    private Random r = new Random();

    public Integer GetEtape()
    {
        return Etape;
    }
    public Integer GetPointServeur()
    {
        return PointServeur;
    }
    public Integer GetPointClient()
    {
        return PointClient;
    }


    public Game(Integer difficulte)
    {
        Log.d("PIERRE PAPIER SPOCK", difficulte .toString());
        if(difficulte<=5&&difficulte>=1)
        {
            this.Diffic=difficulte;
        }
        else{this.Diffic=5;}
    }

    public String play(String Played)//pierre papier ciseaux lezard spock
    {
        Log.d("PIERRE PAPIER SPOCK", Diffic.toString());
        String Resultat;
        if(Etape>=nbrEtapeMax)
        {
            if(PointClient>PointServeur)
            {
                Resultat ="ClientWIN";
            }
            else
            {
                Resultat ="ServerWIN";
            }
            return Resultat;
        }

        switch (Diffic)
        {
            case 1: Resultat= diffOne(Played); break;
            case 2: Resultat= diffTwo(Played); break;
            case 3: Resultat= diffThree(Played); break;
            case 4: Resultat= diffFour(Played); break;
            case 5: Resultat= diffFive(Played); break;
            default: Resultat="error"; break;
        }
        if(Resultat=="error")
        {
            return Resultat;
        }
        else if (Resultat =="égalite")
        {
            return  Resultat;
        }
        Log.d("PIERRE PAPIER SPOCK", Resultat);
        String[] arrOfStr = Resultat.split(":");
        Log.d("PIERRE PAPIER SPOCK", arrOfStr[0]);
        if("win".equalsIgnoreCase(arrOfStr[0]))
        {
            PointClient +=1;
        }
        else if("lose".equalsIgnoreCase(arrOfStr[0]))
        {
            PointServeur+=1;
        }
        else {
            return "error";
        }
        Etape +=1;
        return Resultat;


    }


    private String papier(String played) {
        if(played =="papier") {return  "égalite";}
        if (played == "spock" || played == "pierre") {
            return "lose:papier";
        }
        return  "win:papier";
    }
    private String pierre(String played)
    {
        if(played =="pierre") {return  "égalite";}
        if (played == "lezard" || played == "ciseaux") {
            return "lose:pierre";
        }
        return  "win:pierre";
    }

    private String ciseaux(String played)
    {
        if(played =="ciseaux") {return  "égalite";}
        if (played == "papier" || played == "lezard") {
            return "lose:ciseaux";
        }
        return  "win:ciseaux";
    }

    private String lezard(String played)
    {
        if(played =="lezard") {return  "égalite";}
        if (played == "papier" || played == "spock") {
            return "lose:lezard";
        }
        return  "win:lezard";
    }

    private String spock(String played)
    {
        if(played =="spock") {return  "égalite";}
        if (played == "scissors" || played == "rock") {
            return "lose:spock";
        }
        return  "win:spock";
    }

    private String diffOne(String Played) //en incrémentation
    {
        switch (Etape)
        {
            case 0: return pierre(Played);
            case 1: return papier(Played);
            case 2:return ciseaux(Played);
            case 3: return lezard(Played);
            case 4: return spock(Played);
            default:return  "égalite";
        }
    }

    private String diffTwo(String Played) //66% de chance de gagner
    {
        int randomInt = (r.nextInt(100)+1);
        switch (Played)
        {
            case "pierre":
                if(randomInt<66)
                {
                    return "win:ciseaux";
                }
                return "lose:papier";
            case "papier":
                if(randomInt<66)
                {
                    return "win:pierre";
                }
                return "lose:ciseaux";
            case "ciseaux":
                if(randomInt<66)
                {
                    return "win:papier";
                }
                return "lose:pierre";
            case "lezard":
                if(randomInt<66)
                {
                    return "win:spock";
                }
                return "lose:pierre";
            case "spock":
                if(randomInt<66)
                {
                    return "win:pierre";
                }
                return "lose:lezard";
            default: return "égalite";
        }
    }

    private String diffThree(String Played) //50% de change de gagner
    {
        int randomInt = (r.nextInt(5)+1);
        switch (randomInt)
        {
            case 1: return pierre(Played);
            case 2: return papier(Played);
            case 3:return ciseaux(Played);
            case 4: return lezard(Played);
            case 5: return spock(Played);
            default:return  "égalite";
        }
    }

    private String diffFour(String Played) //33% de chance de gagner
    {
        int randomInt = (r.nextInt(100)+1);
        switch (Played)
        {
            case "pierre":
                if(randomInt<33)
                {
                    return "win:ciseaux";
                }
                return "lose:papier";
            case "papier":
                if(randomInt<33)
                {
                    return "win:pierre";
                }
                return "lose:ciseaux";
            case "ciseaux":
                if(randomInt<33)
                {
                    return "win:papier";
                }
                return "lose:pierre";
            case "lezard":
                if(randomInt<33)
                {
                    return "win:spock";
                }
                return "lose:pierre";
            case "spock":
                if(randomInt<33)
                {
                    return "win:pierre";
                }
                return "lose:lezard";
            default: return "égalite";
        }
    }

    private String diffFive(String Played) //cant Win sauf 1 fois sur 100
    {

        int randomInt = (r.nextInt(100)+1);
        if(randomInt==100)
        {
            randomInt = (r.nextInt(5)+1);
            switch (Etape)
            {
                case 1: return pierre(Played);
                case 2: return papier(Played);
                case 3:return ciseaux(Played);
                case 4: return lezard(Played);
                case 5: return spock(Played);
                default:return  "égalite";
            }
        }
        else
        {
            randomInt = (int)(2.0 % Math.random());
            switch (Played)
            {
                case "pierre":
                    if(randomInt==1)
                    {
                        return "lose:ciseaux";
                    }
                    return "lose:lezard";
                case "papier":
                    if(randomInt==1)
                    {
                        return "lose:pierre";
                    }
                    return "lose:spock";
                case "ciseaux":
                    if(randomInt==1)
                    {
                        return "lose:papier";
                    }
                    return "lose:lezard";
                case "lezard":
                    if(randomInt==1)
                    {
                        return "lose:spock";
                    }
                    return "lose:papier";
                case "spock":
                    if(randomInt==1)
                    {
                        return "lose:pierre";
                    }
                    return "lose:ciseaux";
                default: return "égalite";
            }
        }

    }

}
