package com.myawesomepierrepapierspock.Play;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.myawesomepierrepapierspock.RankActivity;

import java.util.ArrayList;
import java.util.List;

public class Classement {
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String usercollec ="utilisateurs";

    public List<Object[]> getTOP20()
    {
        final List<Object[]> listuser = new ArrayList<Object[]>();
        db.collection(usercollec).orderBy("point", Query.Direction.DESCENDING).limit(20).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful())
                        {
                            for(DocumentSnapshot document : task.getResult()){
                                Object[] swap = new Object[4];
                                swap[0] = document.get("pseudo");
                                swap[1] = document.get("point");
                                swap[0] = document.get("jouer");
                                swap[1] = document.get("perdu");
                                listuser.add(swap);
                            }
                        }
                        else
                        {
                            Log.d("PIERRE PAPIER SPOCK:", "execute: LIST IMPOSSIBLE");
                        }
                    }
                });
        return  listuser;
    }
    public int getRank()
    {
        user= mAuth.getCurrentUser();
        final int[] RANk = new int[1];
        db.collection(usercollec).orderBy("point", Query.Direction.DESCENDING).endAt(user.getUid()).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful())
                        {
                            RANk[0] = task.getResult().size()+1;
                        }
                        else
                        {
                            Log.d("PIERRE PAPIER SPOCK:", "execute: LIST IMPOSSIBLE");
                        }
                    }
                });
        return RANk[0];
    }

}
