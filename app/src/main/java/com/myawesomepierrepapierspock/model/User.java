package com.myawesomepierrepapierspock.model;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Date;

public class User {
    private FirebaseUser user;
    private String ID;
    private String Login;
    private String Mail;
    private String Name;
    private String Sexe;
    private String pseudo;
    private Date Age = new Date();
    private int Point;
    private int jouer;
    private int perdu;

    private int LastLogin;


    public User(FirebaseAuth mAuth)
    {
        this.setUser(mAuth.getCurrentUser());
        if(getUser() != null)
        {
            this.setID(this.getUser().getUid());
            this.setMail(this.getUser().getEmail());
            this.setName(this.getUser().getDisplayName());
        }

    }

    public  User(){};

    public int getPoint() {
        return Point;
    }

    public void setPoint(int ID) {
        this.Point = ID;
    }
    public int getJouer() {
        return jouer;
    }

    public void setJouer(int ID) {
        this.jouer = ID;
    }
    public int getPerdu() {
        return perdu;
    }

    public void setPerdu(int ID) {
        this.perdu = ID;
    }

    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(FirebaseUser user) {
        this.user = user;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String mail) {
        Mail = mail;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSexe() {
        return Sexe;
    }

    public void setSexe(String sexe) {
        Sexe = sexe;
    }

    public int getLastLogin() {
        return LastLogin;
    }

    public void setLastLogin(int lastLogin) {
        LastLogin = lastLogin;
    }

    public Date getAge() {
        return Age;
    }

    public void setAge(Date age) {
        Age = age;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    @NonNull
    @Override
    public String toString() {
        return "name:" + this.getName() + " Mail: " + this.getMail() + " Sexe:" + this.getSexe() + " Pseudo:" + this.getPseudo();
    }
}
