package com.myawesomepierrepapierspock.model;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.Date;
import com.myawesomepierrepapierspock.R;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;


public class Login implements Executor{
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private User utilisateur= new User();
    private String usercollec ="utilisateurs";
    private Boolean Result = Boolean.FALSE;

    public User getUtilisateur() {return  utilisateur;}
    public void setUtilisateur(User utilisateur1) {utilisateur=utilisateur1;}

    public Login()
    {
        mAuth = FirebaseAuth.getInstance();

    }

    public Boolean AlreadySignedin()
    {
        final Boolean[] retour = {false};
         user= mAuth.getCurrentUser();
        if(user!=null)
        {
            user = mAuth.getCurrentUser();
            utilisateur.setMail(user.getEmail());
            utilisateur.setID(user.getUid());
            retour[0] = true;
            DocumentReference docREF = db.collection(usercollec).document(user.getUid());
            docREF.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            final Map<String,Object> userdata = document.getData();
                            try
                            {
                                Timestamp time =(Timestamp)userdata.get("age");
                                utilisateur.setName((String)userdata.get("name"));
                                utilisateur.setAge(time.toDate());
                                utilisateur.setPseudo((String) userdata.get("pseudo"));
                                utilisateur.setSexe((String)userdata.get("sexe"));
                                utilisateur.setMail((String)userdata.get("email"));
                                utilisateur.setPoint((int)(long)userdata.get("point"));
                                utilisateur.setJouer((int)(long)userdata.get("jouer"));
                                utilisateur.setPerdu((int)(long)userdata.get("perdu"));
                                Log.d("profil",utilisateur.toString());
                            }
                            catch (Exception ee)
                            {
                                throw ee;
                            }
                        } else {
                            try {
                                throw new Exception("Error Doc not found");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        retour[0] = false;
                    }
                }
            });
            return  retour[0];
        }
        else
        {
            return  false;
        }
    }

    public Boolean signup(String email, String password, String Name, String pseudo, String sexe, Date Datee)
    {
        final Map<String,Object> userdata = new HashMap<>();
        final Boolean[] retour = {false};
        userdata.put("name",Name); utilisateur.setName(Name);
        userdata.put("age",new Timestamp(Datee)); utilisateur.setAge(Datee);
        userdata.put("pseudo",pseudo); utilisateur.setPseudo(pseudo);
        userdata.put("sexe",sexe);  utilisateur.setSexe(sexe);
        userdata.put("email",email); utilisateur.setMail(email);
        userdata.put("point",0);utilisateur.setPoint(0);
        userdata.put("perdu",0);utilisateur.setJouer(0);
        userdata.put("jouer",0);utilisateur.setPerdu(0);

        try {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener( this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                user = mAuth.getCurrentUser();
                                utilisateur.setID(user.getUid());
                                retour[0] = true;
                                db.collection(usercollec).document(user.getUid())
                                        .set(userdata)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                retour[0] = true;
                                                Log.d("PIERRE PAPIER SPOCK:", "execute: USER CREATED");
                                                Result = true;
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                retour[0] = false;
                                                Log.d("PIERRE PAPIER SPOCK:", "execute: DOC NOT FOUND");
                                            }
                                        });
                            } else {
                                retour[0] = false;
                                user = mAuth.getCurrentUser();
                                Log.d("PIERRE PAPIER SPOCK:", "execute: task not successful" + task.getResult().toString());
                            }
                        }
                    });
        }
        catch (Exception ee)
        {
            throw  ee;
        }
        return retour[0];
    }

    public Boolean signin(String email,String password)
    {
        if(email.isEmpty() || email ==null || password.isEmpty() || password ==null)
        {
            Result = false;
            return Boolean.FALSE;
        }
        final Boolean[] retour = {false,false};
        mAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.d("PIERRE PAPIER SPOCK:", "execute: USER Loged in");
                            user = mAuth.getCurrentUser();
                            retour[0] = true;
                           DocumentReference docREF = db.collection(usercollec).document(user.getUid());
                            docREF.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        DocumentSnapshot document = task.getResult();

                                        if (document.exists()) {
                                            final Map<String,Object> userdata = document.getData();
                                            try
                                            {
                                                Timestamp time =(Timestamp)userdata.get("age");
                                                utilisateur.setName((String)userdata.get("name"));
                                                utilisateur.setAge(time.toDate());
                                                utilisateur.setPseudo((String) userdata.get("pseudo"));
                                                utilisateur.setSexe((String)userdata.get("sexe"));
                                                utilisateur.setMail((String)userdata.get("email"));
                                                utilisateur.setPoint((int)(long)userdata.get("point"));
                                                utilisateur.setJouer((int)(long)userdata.get("jouer"));
                                                utilisateur.setPerdu((int)(long)userdata.get("perdu"));
                                                Log.d("PIERRE PAPIER SPOCK: ", "execute:"+utilisateur.getName());
                                                Result = true;
                                            }
                                            catch (Exception ee)
                                            {
                                                throw ee;
                                            }
                                        } else {
                                            Log.d("PIERRE PAPIER SPOCK:", "execute: DOC NOT FOUND");
                                            try {
                                                throw new Exception("Error Doc not found");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    } else {
                                        retour[0] = false;
                                    }
                                }
                            });
                        }
                        else
                        {
                            Log.d("PIERRE PAPIER SPOCK: ", "execute: SA MARCHE PASSSS");
                            retour[0] = false;
                        }
                    }
                });
        Log.d("PIERRE PAPIER SPOCK: ", "execute:"+ retour[0]);

        return retour[0];
    }

    @Override
    public void execute(Runnable runnable)
    {
        Log.v("STAT", "execute:" + runnable.toString());
        runnable.run();
    }

    public void updatescoreclass()
    {
        DocumentReference docREF = db.collection(usercollec).document(user.getUid());
        docREF.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();

                    if (document.exists()) {
                        final Map<String,Object> userdata = document.getData();
                        try
                        {
                            utilisateur.setPoint((int)(long)userdata.get("point"));
                            utilisateur.setJouer((int)(long)userdata.get("jouer"));
                            utilisateur.setPerdu((int)(long)userdata.get("perdu"));
                        }
                        catch (Exception ee)
                        {
                            throw ee;
                        }
                    } else {
                        Log.d("PIERRE PAPIER SPOCK:", "execute: DOC NOT FOUND");
                        try {
                            throw new Exception("Error Doc not found");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }
    public void AddtoScore(int Point)
    {
        DocumentReference docREF = db.collection(usercollec).document(user.getUid());
        docREF.update("point", Point)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d("PIERRE PAPIER SPOCK:", "Addtoscore: Point ajouter a l'uttilisateur");
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d("PIERRE PAPIER SPOCK:", "Addtoscore: Erreur sur l'ajout de point");
                }
            });
        final int[] partiejouer = new int[1];
        docREF.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();

                    if (document.exists()) {
                        final Map<String, Object> userdata = document.getData();
                        partiejouer[0] = (int)(long)userdata.get("jouer");
                    }
                }
            }
        });
        docREF.update("jouer", (partiejouer[0]+1))
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("PIERRE PAPIER SPOCK:", "Addtoscore: Point ajouter a l'uttilisateur");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("PIERRE PAPIER SPOCK:", "Addtoscore: Erreur sur l'ajout de point");
                    }
                });

    }
    public void PerduPartie()
    {
        DocumentReference docREF = db.collection(usercollec).document(user.getUid());
        final int[] partieperdue = new int[1];
        docREF.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();

                    if (document.exists()) {
                        final Map<String, Object> userdata = document.getData();
                        partieperdue[0] = (int)(long)userdata.get("perdu");
                    }
                }
            }
        });
        docREF.update("perdu", (partieperdue[0]+1))
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("PIERRE PAPIER SPOCK:", "Addtoscore: Point ajouter a l'uttilisateur");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("PIERRE PAPIER SPOCK:", "Addtoscore: Erreur sur l'ajout de point");
                    }
                });
    }

    public Boolean getResult() {
        return Result;
    }

    public void setResult(Boolean result) {
        Result = result;
    }

    public User getUser()
    {
        return  utilisateur;
    }
}
