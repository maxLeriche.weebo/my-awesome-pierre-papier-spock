package com.myawesomepierrepapierspock;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

import androidx.appcompat.app.AppCompatActivity;

import com.myawesomepierrepapierspock.model.Login;

import java.util.Calendar;
import java.util.Date;

public class RegisterActivity extends AppCompatActivity {
    Login MonLogin = new Login();
    private static final String TAG = "RegisterActivity";
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final Button valider = findViewById(R.id.btn_valid);
        final TextView textView = findViewById(R.id.error_txt);
        final String[] date = new String[1];
        mDisplayDate = (TextView) findViewById(R.id.tvDate);

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        RegisterActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mDisplayDate.setText(dayOfMonth + "/" + (month+1) + "/" + year);
                Log.d(TAG, "onDateSet: date: " + year + "/" + month + "/" + dayOfMonth);
            }
        };


        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final EditText mailview = findViewById(R.id.edt_mail);
                final EditText passview = findViewById(R.id.edt_pass);
                final EditText nomview = findViewById(R.id.edt_nom);
                final EditText prenomview = findViewById(R.id.edt_lastname);
                final EditText pseudoview = findViewById(R.id.edt_pseudo);
                final Spinner genderSpinner = findViewById(R.id.gender_spinner);

                final String mail = mailview.getText().toString();
                final String pass = passview.getText().toString();
                final String nom = nomview.getText().toString();
                final String prenom = prenomview.getText().toString();
                final String pseudo = pseudoview.getText().toString();

                String sexe;
                if(genderSpinner.getSelectedItemPosition() == 0)
                {
                    sexe ="Femme";
                }
                else
                {
                    sexe = "Homme";
                }

                date[0] ="";
                DateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date datee = null;
                try {
                    datee = sourceFormat.parse((String) mDisplayDate.getText());
                    Log.d("PIERRE PAPIER SPOCK:", "execute: "+datee);
                } catch (ParseException e) {
                    e.printStackTrace();
                    Log.d("PIERRE PAPIER SPOCK:", "execute: ERROR WITH DATE");
                }
                try {
                    MonLogin.signup(mail,pass,nom+" "+prenom, pseudo, sexe, datee);
                    if (MonLogin.getResult()){

                        Intent menuActivity = new Intent(RegisterActivity.this, MenuActivity.class);
                        startActivity(menuActivity);
                        Log.d("pseudo", MonLogin.getUser().toString());
                        //}else textView.setVisibility(textView.VISIBLE);
                    }
                    else
                    {
                        textView.setText("Register Failed");
                        textView.setVisibility(textView.VISIBLE);
                    }
                }
                catch (Exception ee)
                {
                    textView.setText("Register failed");
                    textView.setVisibility(textView.VISIBLE);
                }

                //Pour mes tests
                //Intent menuActivity = new Intent(RegisterActivity.this, MenuActivity.class);
                //startActivity(menuActivity);
        }});
    }


}
