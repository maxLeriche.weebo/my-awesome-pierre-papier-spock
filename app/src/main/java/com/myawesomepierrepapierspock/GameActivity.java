package com.myawesomepierrepapierspock;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.myawesomepierrepapierspock.Play.Game;
import com.myawesomepierrepapierspock.model.Login;

public class GameActivity extends AppCompatActivity {

    private int tour = 1;
    private int pointCLient = 0;
    private int pointServeur = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Intent lastIntent = getIntent();

        final ImageButton spockbtn = findViewById(R.id.buttonSpock);
        final ImageButton lezardbtn = findViewById(R.id.buttonLezard);
        final ImageButton pierrebtn = findViewById(R.id.buttonPierre);
        final ImageButton feuillebtn = findViewById(R.id.buttonFeuile);
        final ImageButton ciseauxbtn = findViewById(R.id.buttonCiseaux);
        final TextView choice = findViewById(R.id.choiceTxt);

        final Button validate = findViewById(R.id.btn_finTour);
        final TextView error = findViewById(R.id.error_choice);
        final TextView joueurpoint = findViewById(R.id.txt_pointPlayer);
        final TextView IApoint = findViewById(R.id.txt_pointIA);
        final TextView nbround = findViewById(R.id.txt_nbtour);
        final Login useer = new Login();
        useer.AlreadySignedin();
        if (lastIntent.getExtras() != null) {
            //if (lastIntent.getStringExtra("test") !=null )
            //{
            final int diffic = lastIntent.getIntExtra("difficulty", 5);
            //}
            final Game partie = new Game(diffic);

            spockbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    choice.setText("SPOCK");
                }
            });

            feuillebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    choice.setText("FEUILLE");
                }
            });

            pierrebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    choice.setText("PIERRE");
                }
            });

            ciseauxbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    choice.setText("CISEAUX");
                }
            });

            lezardbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    choice.setText("LEZARD");
                }
            });

            validate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String test= "";

                    error.setVisibility(View.INVISIBLE);

                    if (choice.getText().length() > 0){

                        if (choice.getText() == "SPOCK") {
                            test = partie.play("spock");
                        }
                        else if (choice.getText() == "PIERRE") {
                            test = partie.play("pierre");
                        }
                        else if (choice.getText() == "FEUILLE") {
                            test = partie.play("papier");
                        }
                        else if (choice.getText() == "LEZARD") {
                            test = partie.play("lezard");
                        }
                        else if (choice.getText() == "CISEAUX") {
                            test = partie.play("ciseaux");
                        }

                        tour = partie.GetEtape();

                        Log.d("PIERRE PAPIER SPOCK", test);
                        pointCLient = partie.GetPointClient();
                        pointServeur = partie.GetPointServeur();

                        nbround.setText("ROUND : "+tour+"/5");
                        joueurpoint.setText("Joueur : "+pointCLient+"/3");
                        IApoint.setText("IA : "+pointServeur+"/3");
                        if (tour == 5 || pointCLient == 3 || pointServeur == 3){
                            if(pointCLient>=3)
                            {
                                useer.AddtoScore(diffic);
                            }
                            else
                            {
                                useer.PerduPartie();
                            }
                            startActivity(new Intent(GameActivity.this, MenuActivity.class));
                            startActivity(new Intent(GameActivity.this, VictoryActivity.class));
                        }
                    }else error.setVisibility(View.VISIBLE);
                    choice.setText("");
                }
            });
        }
    }
}
