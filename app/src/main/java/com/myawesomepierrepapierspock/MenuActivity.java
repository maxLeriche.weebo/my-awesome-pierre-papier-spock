package com.myawesomepierrepapierspock;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.sql.SQLIntegrityConstraintViolationException;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        final Button valider = findViewById(R.id.btn_lancer);
        final TextView erreur_diffic = findViewById(R.id.erreur_diffic);
        final Button regle = findViewById(R.id.btn_regle);
        final Button profil = findViewById(R.id.btnProfil);
        final Button classement = findViewById(R.id.btn_classement);

        final RadioGroup difficulty = findViewById(R.id.difficultgroup);
        final RadioButton rbfacile = findViewById(R.id.rbfacile);
        final RadioButton rbmoyen = findViewById(R.id.rbmoyen);
        final RadioButton rbdifficile = findViewById(R.id.rbdifficile);
        final RadioButton rbhardcore = findViewById(R.id.rbhardcore);
        final RadioButton rbgodlike = findViewById(R.id.rbgodlike);

        Intent lastIntent = new Intent();

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gameActivity = new Intent(MenuActivity.this, GameActivity.class);
                //Bundle bund = new Bundle();
                RadioButton group_checked = findViewById(difficulty.getCheckedRadioButtonId());
                if (group_checked == null)
                    erreur_diffic.setVisibility(erreur_diffic.VISIBLE);
                else if (group_checked.getText() == rbfacile.getText())
                {
                    //bund.putString("test", "test");
                    //bund.putInt("difficulty", 1);
                    gameActivity.putExtra("difficulty", 1);
                    startActivity(gameActivity);
                }else if (group_checked.getText() == rbmoyen.getText())
                    {
                        //bund.putString("test", "test");
                        //bund.putInt("difficulty", 2);

                        gameActivity.putExtra("difficulty", 2);
                        startActivity(gameActivity);
                    }else if (group_checked.getText() == rbdifficile.getText())
                        {
                            //bund.putString("test", "test");
                            //bund.putInt("difficulty", 3);
                            gameActivity.putExtra("difficulty", 3);
                            startActivity(gameActivity);
                        }else if (group_checked.getText() == rbhardcore.getText())
                            {
                                //bund.putString("test", "test");
                                //bund.putInt("difficulty", 4);
                                gameActivity.putExtra("difficulty", 4);
                                startActivity(gameActivity);
                            }else if (group_checked.getText() == rbgodlike.getText())
                                {
                                    //bund.putString("test", "test");
                                    //bund.putInt("difficulty", 5);
                                    gameActivity.putExtra("difficulty", 5);
                                    startActivity(gameActivity);
                                }
            }
        });

        regle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Redirecting to rules page",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://pausegeek.fr/dossiers/4-apprendre-a-jouer-a-pierre-feuille-ciseaux-lezard-spock.geek")));

            }
        });

        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuActivity.this, ProfilActivity.class));

            }
        });

        classement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuActivity.this, RankActivity.class));
            }
        });
    }
}
