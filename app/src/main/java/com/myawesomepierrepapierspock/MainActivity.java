package com.myawesomepierrepapierspock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.myawesomepierrepapierspock.model.Login;

public class MainActivity extends AppCompatActivity {
    Login MonLogin = new Login();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(MonLogin.AlreadySignedin())
        {
            try{

                Intent MenuActivity = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(MenuActivity);

            }catch (Exception ex){
                Toast.makeText(MainActivity.this, "Erreur", Toast.LENGTH_SHORT).show();
            }
        }
        final Button login, register;
        final EditText name, pass;
        final TextView err;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = findViewById(R.id.btn_login);
        register = findViewById(R.id.btn_register);
        name = findViewById(R.id.edit_name);
        pass = findViewById(R.id.edit_pass);
        err = findViewById(R.id.error_login);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 MonLogin.signin(name.getText().toString(), pass.getText().toString());
                if(MonLogin.getResult())
                {
                    Intent menuActivity = new Intent(MainActivity.this, MenuActivity.class);
                    startActivity(menuActivity);
                }
                Log.d("name",name.getText().toString());
                Log.d("pass",pass.getText().toString());

                //Pour mes tests
                //Intent menuActivity = new Intent(MainActivity.this, MenuActivity.class);
                //startActivity(menuActivity);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    Intent registerActivity = new Intent(MainActivity.this, RegisterActivity.class);
                    startActivity(registerActivity);

                }catch (Exception ex){
                    Toast.makeText(MainActivity.this, "Erreur", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}
